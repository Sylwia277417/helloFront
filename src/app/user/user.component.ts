import { Component, OnInit } from '@angular/core';
import {UserService} from '../user.service';
import {User} from '../user';
import {Task} from '../task';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  users: User[];
  tasks: Task[];

  selectedUser: User;

  constructor(private userService: UserService) { }

  onSelect(user: User): void {
    this.getUserTasks(user.id);
    this.selectedUser = user;
  }

  getUsers(): void {
    this.userService.getUsers()
      .subscribe(users => this.users = users);
  }

  getUserTasks(userId: number): void {
    this.userService.getUserTasks(userId)
      .subscribe(tasks => this.tasks = tasks);
  }

  ngOnInit() {
    this.getUsers();
  }

}
