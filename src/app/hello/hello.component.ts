import { Component, OnInit } from '@angular/core';
import { HelloService } from '../hello.service';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.scss']
})
export class HelloComponent implements OnInit {
  hello: string;

  constructor(private helloService: HelloService) {
  }

  getHello(): void {
    this.helloService.getHello()
      .subscribe(hello => this.hello = hello);
  }

  ngOnInit() {
    this.getHello();
  }

}
