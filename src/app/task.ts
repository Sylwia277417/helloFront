export class Task {
  id: number;
  name: string;
  date: Date;
}
