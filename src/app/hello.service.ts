import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HelloService {

  constructor(private http: HttpClient) { }

  getHello(): Observable<string> {
    const url = 'http://localhost:8881/hello';
    return this.http.get(url, {responseType:  'text'})
      .pipe(
        tap( _ => console.log('hello') ),
        catchError(this.handleError<string>('getUserTasks id='))
      );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(operation + ' ' + error);
      return of(result as T);
    };
  }

}
