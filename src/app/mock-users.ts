import { User } from './user';

export const USERS: User[] = [
  { id: 11, name: 'Mr. Nice', surname: 'A' },
  { id: 12, name: 'Narco', surname: 'B' },
  { id: 13, name: 'Bombasto', surname: 'C' },
  { id: 14, name: 'Celeritas', surname: 'D' },
  { id: 15, name: 'Magneta', surname: 'E' },
  { id: 16, name: 'RubberMan', surname: 'F' },
  { id: 17, name: 'Dynama', surname: 'G' },
  { id: 18, name: 'Dr IQ', surname: 'H' },
  { id: 19, name: 'Magma', surname: 'I' },
  { id: 20, name: 'Tornado', surname: 'J' }
]
