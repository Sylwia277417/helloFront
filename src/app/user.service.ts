import { Injectable } from '@angular/core';
import { User } from './user';
import {Observable, of} from 'rxjs';
import { HttpClient } from '@angular/common/http';
import {Task} from './task';
import {catchError, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient ) { }

  getUsers(): Observable<User[]> {
    const url = 'http://localhost:8881/user/list';
    return this.http.get<User[]>(url)
      .pipe(
        tap( _ => console.log('getUsers list') ),
        catchError(this.handleError('getUsers', []))
      );
  }

  getUserTasks(userId: number): Observable<Task[]> {
    const url = 'http://localhost:8881/user/' + userId + '/task/list';
    return this.http.get<Task[]>(url)
      .pipe(
        tap( _ => console.log('getUserTasks for id=' + userId) ),
        catchError(this.handleError('getUserTasks id=' + userId, []))
      );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(operation + ' ' + error);
      return of(result as T);
    };
  }

}
